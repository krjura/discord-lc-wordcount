#!/usr/bin/env bash

./gradlew -no-daemon build bootJar

docker build -t docker.krjura.org/lara-cimbal/wordcount:v7 .