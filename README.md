# Reasoning

This application is a prototype of a discord bot for my niece's fantasy role-playing group.
The boot needs to count how many words each participant sends in a single post. 
If the number of words is below a certain threshold it sends a warning to the sender.

# Commands

Bot supports the following commands:

* status - shows configuration for this channel
* check - activate check for this channel
* no check - de-activate check for this channel
* set min words <number> - set <number> as minimum number of words in the text 

# Installation

In order to use this boot it has to be registered with discord first.
To register the bot, go to https://discord.com/developers/applications and select a "New Application" button.
This will open a new dialog. In the dialog specify the name of the application and click on "Create".
Once the application is created, select the application in the "My Applications".
In the menu on the left side select "Bot". 
This will open the page for management of the application bot.
Each application can have only one bot. 
Select "Add Bot" button. 
Discord will automatically create the bot and its access credentials.
To access the bot's token click on "Click to Reveal Token" and write it down.
This token should be saved in "krjura.discord.lcwordcount.token" configuration parameter or "KRJURA_DISCORD_LCWORDCOUNT_TOKEN" environment variable.

Next step is to generate the OAuth 2.0 URL that will be used to add the bot to any channel.
to generate the OAuth 2.0 URL select the "OAuth2" menu item on the left.
In the OAuth2 page select "bot" under scopes and "Send Messages" under bot permissions.
Copy the generated URL and send it to the server owner.

When the server owner enters this URL in the browser, discord will ask the owner to which server the bot should be added.  



# GC tuning

```
-Xmx256m -XX:+UseG1GC -XX:MaxGCPauseMillis=200 -Xlog:gc=info:file=/tmp/gc.log:time:filecount=5,filesize=10m -Djava.security.egd=file:/dev/./urandom
```