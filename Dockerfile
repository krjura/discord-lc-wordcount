FROM adoptopenjdk/openjdk14:x86_64-alpine-jre-14.0.2_12

LABEL maintainer="Krešimir Jurasović <krjura@outlook.com>"

ENV APPLICATION_HOME=/opt/discord \
    APPLICATION_USER=discord \
    APPLICATION_GROUP=discord

RUN adduser --disabled-password --no-create-home --shell /sbin/nologin --uid 1200 $APPLICATION_USER

RUN mkdir -p $APPLICATION_HOME/logs \
    && mkdir -p $APPLICATION_HOME/tmp \
    && mkdir -p $APPLICATION_HOME/running \
    && chown -R $APPLICATION_USER:$APPLICATION_GROUP $APPLICATION_HOME \
    && chmod -R a-w,o-rwx $APPLICATION_HOME \
    && chmod -R ug+w $APPLICATION_HOME/running $APPLICATION_HOME/logs $APPLICATION_HOME/tmp

COPY --chown=$APPLICATION_USER:$APPLICATION_GROUP build/libs/application-boot.jar $APPLICATION_HOME/application-boot.jar

WORKDIR /opt/discord
USER $APPLICATION_USER

EXPOSE 25000

CMD java $JAVA_OPTS -jar application-boot.jar