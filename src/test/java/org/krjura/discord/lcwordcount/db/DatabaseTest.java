package org.krjura.discord.lcwordcount.db;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.krjura.discord.lcwordcount.config.AppConfig;
import org.krjura.discord.lcwordcount.ex.ApplicationException;
import org.krjura.discord.lcwordcount.utils.ContentUtils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class DatabaseTest {

    @Test
    public void createDatabaseSuccessfully() throws Exception {
        var config = new AppConfig()
                .backupLocation("/tmp/" + UUID.randomUUID().toString())
                .token("token");

        var database = new Database(config);

        database.setGuildSelfIds("400000000000000000", List.of("400000000000000001", "400000000000000002"));
        database.setMinWords("500000000000000000", 30);
        database.registerChannel("500000000000000001");

        assertThat(database.getGuildSelfIds("400000000000000000")).hasSize(2);
        assertThat(database.getGuildSelfIds("400000000000000000")).contains("400000000000000001");
        assertThat(database.getGuildSelfIds("400000000000000000")).contains("400000000000000002");

        assertThat(database.getMinWords("500000000000000000")).isEqualTo(30);
        assertThat(database.getMinWords("500000000000000001")).isEqualTo(50);
        assertThat(database.getMinWords("500000000000000002")).isEqualTo(50);

        assertThat(database.isChannelRegistered("500000000000000001")).isTrue();
        assertThat(database.isChannelRegistered("500000000000000002")).isFalse();
    }

    @Test
    public void loadDatabaseSuccessfully() throws Exception {
        var config = new AppConfig()
                .backupLocation("/tmp/" + UUID.randomUUID().toString())
                .token("token");

        writeValidDbToBackupLocation(config);

        var database = new Database(config);

        assertThat(database.getGuildSelfIds("100000000000000000")).hasSize(1);
        assertThat(database.getGuildSelfIds("100000000000000000")).contains("100000000000000001");
        assertThat(database.getGuildSelfIds("200000000000000000")).hasSize(2);
        assertThat(database.getGuildSelfIds("200000000000000000")).contains("200000000000000001");
        assertThat(database.getGuildSelfIds("200000000000000000")).contains("200000000000000002");

        assertThat(database.isChannelRegistered("300000000000000000")).isFalse();
        assertThat(database.isChannelRegistered("300000000000000001")).isTrue();
        assertThat(database.isChannelRegistered("300000000000000002")).isFalse();

        assertThat(database.getMinWords("300000000000000000")).isEqualTo(10);
        assertThat(database.getMinWords("300000000000000001")).isEqualTo(20);
        assertThat(database.getMinWords("300000000000000002")).isEqualTo(50);
    }

    @Test
    public void unregisterTheChannelSuccessfully() throws Exception {
        var config = new AppConfig()
                .backupLocation("/tmp/" + UUID.randomUUID().toString())
                .token("token");

        var database = new Database(config);

        database.registerChannel("300000000000000000");
        assertThat(database.isChannelRegistered("300000000000000000")).isTrue();

        database.unRegisterChannel("300000000000000000");
        assertThat(database.isChannelRegistered("300000000000000000")).isFalse();
    }

    @Test
    public void failOnInvalidDatabaseJson() throws Exception {
        var config = new AppConfig()
                .backupLocation("/tmp/" + UUID.randomUUID().toString())
                .token("token");

        writeInvalidDbToBackupLocation(config);

        assertThrows(ApplicationException.class, () -> new Database(config));
    }

    @Test
    public void ensureBackupWorks() throws Exception {
        // prepare

        var config = new AppConfig()
                .backupLocation("/tmp/" + UUID.randomUUID().toString())
                .token("token");

        var database = new Database(config);

        database.setGuildSelfIds("400000000000000000", List.of("400000000000000001", "400000000000000002"));
        database.setMinWords("500000000000000000", 30);
        database.registerChannel("500000000000000001");

        // execute
        database.backup();

        // verify

        DatabaseData savedData = new ObjectMapper()
                .readValue(new File(config.getBackupLocation()), DatabaseData.class);

        var channels = savedData.getChannelRegistrations();
        var guilds  = savedData.getGuildSelfIds();

        assertThat(channels).isNotNull();

        assertThat(channels.get("500000000000000000")).isNotNull();
        assertThat(channels.get("500000000000000000").getMinWords()).isEqualTo(30);

        assertThat(channels.get("500000000000000001")).isNotNull();
        assertThat(channels.get("500000000000000001").isRegistered()).isTrue();

        assertThat(guilds).isNotNull();

        assertThat(guilds.get("400000000000000000")).isNotNull();
        assertThat(guilds.get("400000000000000000")).contains("400000000000000001");
        assertThat(guilds.get("400000000000000000")).contains("400000000000000002");
    }

    private void writeValidDbToBackupLocation(AppConfig config) throws IOException {
        byte[] content = ContentUtils.loadFromClassPath("database/databaseTest-loadDatabaseSuccessfully.json");

        Files.write(Path.of(config.getBackupLocation()), content);
    }

    private void writeInvalidDbToBackupLocation(AppConfig config) throws IOException {
        byte[] content = ContentUtils.loadFromClassPath("database/databaseTest-failOnInvalidDatabaseJson.json");

        Files.write(Path.of(config.getBackupLocation()), content);
    }
}