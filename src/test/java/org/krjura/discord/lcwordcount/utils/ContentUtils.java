package org.krjura.discord.lcwordcount.utils;

import java.nio.file.Files;
import java.nio.file.Paths;
import org.krjura.discord.lcwordcount.support.TestingException;

public class ContentUtils {

    public static byte[] loadFromClassPath(String fileName) {
        try {
            return Files.readAllBytes(Paths.get(ContentUtils.class.getClassLoader()
                    .getResource(fileName)
                    .toURI()));
        } catch ( Exception  e) {
            throw new TestingException("Cannot read bytes of file " + fileName , e);
        }
    }
}
