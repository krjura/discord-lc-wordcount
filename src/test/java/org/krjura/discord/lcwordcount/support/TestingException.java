package org.krjura.discord.lcwordcount.support;

public class TestingException extends RuntimeException {

    public TestingException(String s) {
        super(s);
    }

    public TestingException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
