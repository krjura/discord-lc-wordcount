package org.krjura.discord.lcwordcount.discord.actions.specific;

import java.util.List;
import org.junit.jupiter.api.Test;
import org.krjura.discord.lcwordcount.pojo.MessageData;
import org.mockito.ArgumentCaptor;

import static org.assertj.core.api.Assertions.assertThat;
import static org.krjura.discord.lcwordcount.discord.utils.TestUtils.mockSendMessage;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class CommandsActionTest {

    private static final String SELF_ROLE_ID = "statusRole1";
    private static final String CHANNEL_ID = "123456789";

    @Test
    public void verifySupportWhenNotMentioned() {
        var action = new CommandsAction();

        var roleOneData = MessageData
                .of()
                .selfGuildRoleIds(List.of(SELF_ROLE_ID))
                .roleMentionIds(List.of("not help"));
        assertThat(action.supports(roleOneData)).isFalse();
    }

    @Test
    public void verifySupportWhenMentionedButNoCommand() {
        var action = new CommandsAction();

        var roleOneData = MessageData
                .of()
                .selfGuildRoleIds(List.of(SELF_ROLE_ID))
                .roleMentionIds(List.of(SELF_ROLE_ID))
                .content("not help content")
                .sanitizedContent("some help content");

        assertThat(action.supports(roleOneData)).isFalse();
    }

    @Test
    public void verifySupportWhenMentionedAndCommand() {
        var action = new CommandsAction();

        var roleOneData = MessageData
                .of()
                .selfGuildRoleIds(List.of(SELF_ROLE_ID))
                .roleMentionIds(List.of(SELF_ROLE_ID))
                .content("help")
                .sanitizedContent("help");

        assertThat(action.supports(roleOneData)).isTrue();
    }

    @Test
    public void verifyExecute() {
        var action = new CommandsAction();

        var sendMessageMocks = mockSendMessage();

        var data = MessageData
                .of()
                .message(sendMessageMocks.getMessage())
                .selfGuildRoleIds(List.of(SELF_ROLE_ID))
                .roleMentionIds(List.of(SELF_ROLE_ID))
                .channelId(CHANNEL_ID)
                .content("help")
                .sanitizedContent("help");

        assertThat(action.execute(data).block()).isSameAs(data);

        ArgumentCaptor<String> textCaptor = ArgumentCaptor.forClass(String.class);
        verify(sendMessageMocks.getMessageChannel(), times(1)).createMessage(textCaptor.capture());

        assertThat(textCaptor.getValue()).contains("Bot supports the following commands");
    }
}