package org.krjura.discord.lcwordcount.discord.actions.specific;

import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.krjura.discord.lcwordcount.db.Database;
import org.krjura.discord.lcwordcount.pojo.MessageData;
import org.mockito.ArgumentCaptor;

import static org.assertj.core.api.Assertions.assertThat;
import static org.krjura.discord.lcwordcount.discord.utils.TestUtils.mockSendMessage;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class ChangeMinWordsActionTest {

    private static final String SELF_ROLE_ID_1 = "selfRoleId1";

    private Database database;
    private ChangeMinWordsAction action;

    @BeforeEach
    public void before() {
        this.database = mock(Database.class);
        this.action = new ChangeMinWordsAction(this.database);
    }

    @Test
    public void verifySupportWhenNotMentioned() {
        var roleOneData = MessageData
                .of()
                .selfGuildRoleIds(List.of(SELF_ROLE_ID_1))
                .roleMentionIds(List.of("notexists"));
        assertThat(action.supports(roleOneData)).isFalse();
    }

    @Test
    public void verifySupportWhenMentionedButNoCommand() {
        var roleOneData = MessageData
                .of()
                .selfGuildRoleIds(List.of(SELF_ROLE_ID_1))
                .roleMentionIds(List.of(SELF_ROLE_ID_1))
                .content("some content")
                .sanitizedContent("some content");

        assertThat(action.supports(roleOneData)).isFalse();
    }

    @Test
    public void verifySupportWhenMentionedAndCommand() {
        var roleOneData = MessageData
                .of()
                .selfGuildRoleIds(List.of(SELF_ROLE_ID_1))
                .roleMentionIds(List.of(SELF_ROLE_ID_1))
                .content("set min words")
                .sanitizedContent("set min words");

        assertThat(action.supports(roleOneData)).isTrue();
    }

    @Test
    public void verifyExecuteWhenMinWordsBelowOne() {
        var sendMessageMocks = mockSendMessage();

        var data = MessageData
                .of()
                .message(sendMessageMocks.getMessage())
                .selfGuildRoleIds(List.of(SELF_ROLE_ID_1))
                .roleMentionIds(List.of(SELF_ROLE_ID_1))
                .content("set min words -1")
                .sanitizedContent("set min words -1");


        assertThat(action.execute(data).block()).isSameAs(data);

        ArgumentCaptor<String> textCaptor = ArgumentCaptor.forClass(String.class);
        verify(sendMessageMocks.getMessageChannel(), times(1)).createMessage(textCaptor.capture());

        assertThat(textCaptor.getValue()).isEqualTo("<@694289030983974983> number must be larger then 0");
    }

    @Test
    public void verifyExecuteWhenMinWordsNotANumber() {
        var sendMessageMocks = mockSendMessage();

        var data = MessageData
                .of()
                .message(sendMessageMocks.getMessage())
                .selfGuildRoleIds(List.of(SELF_ROLE_ID_1))
                .roleMentionIds(List.of(SELF_ROLE_ID_1))
                .content("set min words aa")
                .sanitizedContent("set min words aa");


        assertThat(action.execute(data).block()).isSameAs(data);

        ArgumentCaptor<String> textCaptor = ArgumentCaptor.forClass(String.class);
        verify(sendMessageMocks.getMessageChannel(), times(1)).createMessage(textCaptor.capture());

        assertThat(textCaptor.getValue()).isEqualTo("<@694289030983974983> aa is not a number");
    }

    @Test
    public void verifyExecuteSuccessfully() {
        var sendMessageMocks = mockSendMessage();

        var data = MessageData
                .of()
                .message(sendMessageMocks.getMessage())
                .selfGuildRoleIds(List.of(SELF_ROLE_ID_1))
                .roleMentionIds(List.of(SELF_ROLE_ID_1))
                .content("set min words 1")
                .sanitizedContent("set min words 1");


        assertThat(action.execute(data).block()).isSameAs(data);

        ArgumentCaptor<String> textCaptor = ArgumentCaptor.forClass(String.class);
        verify(sendMessageMocks.getMessageChannel(), times(1)).createMessage(textCaptor.capture());

        assertThat(textCaptor.getValue()).isEqualTo("<@694289030983974983> min words is now 1");
    }

}