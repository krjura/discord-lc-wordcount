package org.krjura.discord.lcwordcount.discord.actions;

import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.krjura.discord.lcwordcount.config.AppConfig;
import org.krjura.discord.lcwordcount.db.Database;
import org.krjura.discord.lcwordcount.pojo.MessageData;
import org.mockito.ArgumentCaptor;

import static org.assertj.core.api.Assertions.assertThat;
import static org.krjura.discord.lcwordcount.discord.utils.TestUtils.mockSendMessage;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class CountWordsActionTest {

    private Database database;
    private CountWordsAction action;

    @BeforeEach
    public void before() {
        var config = new AppConfig()
                .backupLocation("/tmp/" + UUID.randomUUID().toString())
                .token("token");

        this.database = new Database(config);
        this.action = new CountWordsAction(database);
    }

    @Test
    public void countWordsSuccessfully() {
        String channelId = UUID.randomUUID().toString();
        var sendMessageMocks = mockSendMessage();
        this.database.setMinWords(channelId, 1);

        var data = new MessageData()
                .content("this is a word test")
                .message(sendMessageMocks.getMessage())
                .channelId(channelId);

        assertThat(action.execute(data).block()).isSameAs(data);

        // verify not message are send
        ArgumentCaptor<String> textCaptor = ArgumentCaptor.forClass(String.class);
        verify(sendMessageMocks.getMessageChannel(), times(0)).createMessage(textCaptor.capture());
    }

    @Test
    public void countWordsWhenBelowLimit() {
        String channelId = UUID.randomUUID().toString();
        var sendMessageMocks = mockSendMessage();
        this.database.setMinWords(channelId, 50);

        var data = new MessageData()
                .content("this is a word test")
                .message(sendMessageMocks.getMessage())
                .channelId(channelId);

        assertThat(action.execute(data).block()).isSameAs(data);

        // verify not message are send
        ArgumentCaptor<String> textCaptor = ArgumentCaptor.forClass(String.class);
        verify(sendMessageMocks.getMessageChannel(), times(1)).createMessage(textCaptor.capture());
        assertThat(textCaptor.getValue())
                .contains("<@694289030983974983>Too little words in this post expected 50 but got 5");
    }
}