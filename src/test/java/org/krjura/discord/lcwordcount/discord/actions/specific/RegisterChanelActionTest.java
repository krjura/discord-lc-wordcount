package org.krjura.discord.lcwordcount.discord.actions.specific;

import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.krjura.discord.lcwordcount.db.Database;
import org.krjura.discord.lcwordcount.pojo.MessageData;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThat;
import static org.krjura.discord.lcwordcount.discord.utils.TestUtils.mockSendMessage;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class RegisterChanelActionTest {

    private static final String SELF_ROLE_ID = "statusRole1";
    private static final String CHANNEL_ID = "123456789";

    private Database database;

    @BeforeEach
    public void before() {
        this.database = mock(Database.class);
    }

    @Test
    public void verifySupportWhenNotMentioned() {

        var action = new RegisterChanelAction(this.database);

        var roleOneData = MessageData
                .of()
                .selfGuildRoleIds(List.of(SELF_ROLE_ID))
                .roleMentionIds(List.of("not check"));
        assertThat(action.supports(roleOneData)).isFalse();
    }

    @Test
    public void verifySupportWhenMentionedButNoCommand() {
        var action = new RegisterChanelAction(this.database);

        var roleOneData = MessageData
                .of()
                .selfGuildRoleIds(List.of(SELF_ROLE_ID))
                .roleMentionIds(List.of(SELF_ROLE_ID))
                .content("not check content")
                .sanitizedContent("some check content");

        assertThat(action.supports(roleOneData)).isFalse();
    }

    @Test
    public void verifySupportWhenMentionedAndCommand() {
        var action = new RegisterChanelAction(this.database);

        var roleOneData = MessageData
                .of()
                .selfGuildRoleIds(List.of(SELF_ROLE_ID))
                .roleMentionIds(List.of(SELF_ROLE_ID))
                .content("check")
                .sanitizedContent("check");

        assertThat(action.supports(roleOneData)).isTrue();
    }

    @Test
    public void verifyExecute() {
        var action = new RegisterChanelAction(this.database);

        var sendMessageMocks = mockSendMessage();

        var data = MessageData
                .of()
                .message(sendMessageMocks.getMessage())
                .selfGuildRoleIds(List.of(SELF_ROLE_ID))
                .roleMentionIds(List.of(SELF_ROLE_ID))
                .channelId(CHANNEL_ID)
                .content("check")
                .sanitizedContent("check");

        assertThat(action.execute(data).block()).isSameAs(data);

        ArgumentCaptor<String> textCaptor = ArgumentCaptor.forClass(String.class);
        verify(sendMessageMocks.getMessageChannel(), times(1)).createMessage(textCaptor.capture());

        assertThat(textCaptor.getValue()).contains("This channel will be checked for word length");
        verify(this.database, times(1)).registerChannel(CHANNEL_ID);
    }

}