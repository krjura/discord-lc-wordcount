package org.krjura.discord.lcwordcount.discord.actions.fallback;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.krjura.discord.lcwordcount.db.Database;
import org.krjura.discord.lcwordcount.pojo.MessageData;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class IgnoreUnregisteredChannelActionTest {

    private static final String CHANNEL_ID = "testId";
    private static final String CHANNEL_ID_OTHER = "testId2";

    private Database database;

    private IgnoreUnregisteredChannelAction action;

    @BeforeEach
    public void before() {
        this.database = mock(Database.class);
        this.action = new IgnoreUnregisteredChannelAction(this.database);
    }

    @Test
    public void verifySupportWhenChannelIdIsFoundInTheDatabase() {
        var data = MessageData
                .of()
                .channelId(CHANNEL_ID);

        when(database.isChannelRegistered(CHANNEL_ID)).thenReturn(true);
        assertThat(action.supports(data)).isFalse();
    }

    @Test
    public void verifySupportWhenOtherChannelIdIsFoundInTheDatabase() {
        var data = MessageData
                .of()
                .channelId(CHANNEL_ID);

        when(database.isChannelRegistered(CHANNEL_ID_OTHER)).thenReturn(true);
        assertThat(action.supports(data)).isTrue();
    }

    @Test
    public void verifySupportWhenChannelIdNotIsFoundInTheDatabase() {
        var data = MessageData.of();

        when(database.isChannelRegistered(CHANNEL_ID)).thenReturn(false);
        assertThat(action.supports(data)).isTrue();
    }

    @Test
    public void verifyExecuteReturnsSameObject() {
        var data = MessageData.of();

        assertThat(action.execute(data).block()).isSameAs(data);
    }

}