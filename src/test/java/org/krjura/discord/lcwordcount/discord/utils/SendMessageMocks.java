package org.krjura.discord.lcwordcount.discord.utils;

import discord4j.core.GatewayDiscordClient;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.channel.MessageChannel;
import discord4j.discordjson.json.MessageData;
import discord4j.discordjson.json.UserData;

public class SendMessageMocks {

    private final MessageChannel messageChannel;
    private final GatewayDiscordClient client;
    private final MessageData messageData;
    private final UserData userData;
    private final Message message;

    public SendMessageMocks(
            MessageChannel messageChannel,
            GatewayDiscordClient client,
            MessageData messageData,
            UserData userData,
            Message message) {

        this.messageChannel = messageChannel;
        this.client = client;
        this.messageData = messageData;
        this.userData = userData;
        this.message = message;
    }

    public MessageChannel getMessageChannel() {
        return messageChannel;
    }

    public GatewayDiscordClient getClient() {
        return client;
    }

    public MessageData getMessageData() {
        return messageData;
    }

    public UserData getUserData() {
        return userData;
    }

    public Message getMessage() {
        return message;
    }
}