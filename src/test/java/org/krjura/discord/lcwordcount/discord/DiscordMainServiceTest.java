package org.krjura.discord.lcwordcount.discord;

import discord4j.common.util.Snowflake;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.User;
import discord4j.discordjson.json.MemberData;
import discord4j.discordjson.json.UserData;
import discord4j.discordjson.possible.Possible;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.krjura.discord.lcwordcount.config.AppConfig;
import org.krjura.discord.lcwordcount.db.Database;
import org.krjura.discord.lcwordcount.discord.actions.ActionService;
import org.krjura.discord.lcwordcount.discord.actions.DefaultAction;
import org.krjura.discord.lcwordcount.pojo.MessageData;
import org.mockito.Mockito;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DiscordMainServiceTest {

    private static final String CONST_GUILD_ID = "200000000000000000";
    private static final long CONST_GUILD_ID_LONG = 200000000000000000L;

    private AppConfig config;
    private Database database;

    @BeforeEach
    public void init() {
        config = new AppConfig()
                .backupLocation("/tmp/" + UUID.randomUUID().toString())
                .token("token");

        this.database = new Database(config);
    }

    @Test
    public void verifyFlow() {
        var action = mock(TestDefaultAction.class);
        var actionService = new ActionService(List.of(), List.of(), action);
        var client = mock(GatewayDiscordClient.class);
        var userData = mock(UserData.class);
        var user = new User(client, userData);
        var event = mock(MessageCreateEvent.class);
        var discordMessageData = mock(discord4j.discordjson.json.MessageData.class);
        var memberData = mock(MemberData.class);
        when(memberData.user()).thenReturn(userData);
        var member = new Member(client, memberData, CONST_GUILD_ID_LONG);

        // mock
        when(client.getSelf()).thenReturn(Mono.just(user));

        when(userData.id()).thenReturn("100000000000000001");
        when(userData.username()).thenReturn("DiscordMainServiceTest");
        when(userData.bot()).thenReturn(Possible.of(false));

        when(discordMessageData.content()).thenReturn("content");
        when(discordMessageData.mentionRoles()).thenReturn(List.of("100000000000000001"));
        when(discordMessageData.id()).thenReturn("100000000000000001");
        when(discordMessageData.channelId()).thenReturn("100000000000000002");
        when(discordMessageData.webhookId()).thenReturn(Possible.absent());
        when(discordMessageData.author()).thenReturn(userData);
        when(discordMessageData.guildId()).thenReturn(Possible.of(CONST_GUILD_ID));

        when(client.getMemberById(any(Snowflake.class), any(Snowflake.class))).thenReturn(Mono.just(member));
        when(client.on(MessageCreateEvent.class)).thenReturn(Flux.just(event));

        var message = new Message(client, discordMessageData);
        when(event.getMessage()).thenReturn(message);

        when(action.execute(any(MessageData.class))).thenReturn(Mono.just(MessageData.of()));

        // execute
        var service = new DiscordMainService(this.database, actionService, client);
        service.init();

        verify(action, times(1)).execute(any(MessageData.class));
    }

    private static class TestDefaultAction implements DefaultAction {

        @Override
        public Mono<MessageData> execute(MessageData data) {
            return Mono.just(MessageData.of().content("default"));
        }
    }
}