package org.krjura.discord.lcwordcount.discord.utils;

import discord4j.core.GatewayDiscordClient;
import discord4j.core.object.entity.Message;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThat;
import static org.krjura.discord.lcwordcount.discord.utils.MessageDataUtils.initialize;
import static org.krjura.discord.lcwordcount.discord.utils.MessageDataUtils.sanitizeMessageContent;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MessageDataUtilsTest {

    @Test
    public void initializeMessageData() {
        var client = Mockito.mock(GatewayDiscordClient.class);
        var discordMessageData = mock(discord4j.discordjson.json.MessageData.class);

        when(discordMessageData.id()).thenReturn("100000000000000000");
        when((discordMessageData.content())).thenReturn("content");
        when(discordMessageData.mentionRoles()).thenReturn(List.of("100000000000000001", "100000000000000002"));
        when(discordMessageData.channelId()).thenReturn("100000000000000003");

        Message message = new Message(client, discordMessageData);

        var data = initialize(message);
        assertThat(data).isNotNull();
        assertThat(data.getMessageId()).isEqualTo("100000000000000000");
        assertThat(data.getMessage()).isEqualTo(message);
        assertThat(data.getContent()).isEqualTo("content");
        assertThat(data.getSanitizedContent()).isEqualTo("content");
        assertThat(data.getRoleMentionIds()).containsAll(List.of("100000000000000001", "100000000000000002"));
        assertThat(data.getChannelId()).isEqualTo("100000000000000003");
    }

    @Test
    public void sanitizeMessage() {

        assertThat(sanitizeMessageContent("<@&739528644531191835> bok")).isEqualTo("bok");
        assertThat(sanitizeMessageContent("<@!739528644531191835> bok")).isEqualTo("bok");
        assertThat(sanitizeMessageContent("<@739528644531191835> bok")).isEqualTo("bok");
    }
}