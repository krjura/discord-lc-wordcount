package org.krjura.discord.lcwordcount.discord.actions.specific;

import org.junit.jupiter.api.Test;
import org.krjura.discord.lcwordcount.pojo.MessageData;

import static org.assertj.core.api.Assertions.assertThat;

public class IgnoreBotsTest {

    @Test
    public void verifyWhenBot() {
        var data = MessageData.of().userBot(false);

        assertThat(new IgnoreBots().supports(data)).isFalse();
    }

    @Test
    public void verifyWhenNotBot() {
        var data = MessageData.of().userBot(true);

        assertThat(new IgnoreBots().supports(data)).isTrue();
    }

    @Test
    public void verifyExecute() {
        var data = MessageData.of();

        assertThat(new IgnoreBots().execute(data).block()).isSameAs(data);
    }

}