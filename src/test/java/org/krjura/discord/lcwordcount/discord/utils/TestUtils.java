package org.krjura.discord.lcwordcount.discord.utils;

import discord4j.common.util.Snowflake;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.channel.MessageChannel;
import discord4j.discordjson.json.UserData;
import reactor.core.publisher.Mono;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestUtils {

    public static SendMessageMocks mockSendMessage() {
        var channel = mock(MessageChannel.class);
        var client = mock(GatewayDiscordClient.class);
        var discordMessageData = mock(discord4j.discordjson.json.MessageData.class);
        var userData = mock(UserData.class);

        when(discordMessageData.channelId()).thenReturn("694289030983974981");
        when(discordMessageData.id()).thenReturn("694289030983974982");
        when(discordMessageData.author()).thenReturn(userData);
        when(userData.id()).thenReturn("694289030983974983");
        when(client.getChannelById(Snowflake.of("694289030983974981"))).thenReturn(Mono.just(channel));

        var message = new Message(client, discordMessageData);

        when(channel.createMessage(anyString())).thenReturn(Mono.just(message));

        return new SendMessageMocks(channel, client, discordMessageData, userData, message);
    }
}
