package org.krjura.discord.lcwordcount.discord.actions.fallback;

import java.util.List;
import org.junit.jupiter.api.Test;
import org.krjura.discord.lcwordcount.pojo.MessageData;

import static org.assertj.core.api.Assertions.assertThat;
import static org.krjura.discord.lcwordcount.discord.utils.TestUtils.mockSendMessage;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class SelfMentionActionTest {

    private static final String SELF_ROLE_ID_1 = "selfRoleId1";
    private static final String SELF_ROLE_ID_2 = "selfRoleId2";

    @Test
    public void verifySupportsSelfMentionWhenTheyExist() {
        var action = new SelfMentionAction();

        var roleOneData = MessageData
                .of()
                .selfGuildRoleIds(List.of(SELF_ROLE_ID_1, SELF_ROLE_ID_2))
                .roleMentionIds(List.of(SELF_ROLE_ID_1));
        assertThat(action.supports(roleOneData)).isTrue();

        var roleTwoData = MessageData
                .of()
                .selfGuildRoleIds(List.of(SELF_ROLE_ID_1, SELF_ROLE_ID_2))
                .roleMentionIds(List.of(SELF_ROLE_ID_2));
        assertThat(action.supports(roleTwoData)).isTrue();
    }

    @Test
    public void verifySupportsSelfMentionWhenTheyDontExist() {
        var action = new SelfMentionAction();

        var roleOneData = MessageData
                .of()
                .selfGuildRoleIds(List.of(SELF_ROLE_ID_1, SELF_ROLE_ID_2))
                .roleMentionIds(List.of("not-exists"));
        assertThat(action.supports(roleOneData)).isFalse();
    }

    @Test
    public void verifyExecute() {
        var sendMessageMocks = mockSendMessage();

        var data = MessageData
                .of()
                .message(sendMessageMocks.getMessage())
                .content("check")
                .sanitizedContent("check");

        var action = new SelfMentionAction();

        assertThat(action.execute(data).block()).isSameAs(data);
        verify(sendMessageMocks.getMessageChannel(), times(1)).createMessage(anyString());
    }
}