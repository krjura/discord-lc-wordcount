package org.krjura.discord.lcwordcount.discord.actions;

import java.util.List;
import java.util.Objects;
import org.junit.jupiter.api.Test;
import org.krjura.discord.lcwordcount.discord.actions.fallback.FallbackAction;
import org.krjura.discord.lcwordcount.discord.actions.specific.SpecificAction;
import org.krjura.discord.lcwordcount.pojo.MessageData;
import reactor.core.publisher.Mono;

import static org.assertj.core.api.Assertions.assertThat;
import static java.util.Objects.requireNonNull;

public class ActionServiceTest {

    private static final DefaultAction defaultAction = new TestDefaultAction();

    @Test
    public void mustMatchSpecific() {
        List<SpecificAction> specifics = List.of(new TestSpecific(true, "test1"));
        List<FallbackAction> fallback = List.of();

        var action = new ActionService(specifics, fallback, defaultAction);
        var result = action.resolve(MessageData.of()).block();

        assertThat(result).isNotNull();
        assertThat(result.getContent()).isEqualTo("test1");
    }

    @Test
    public void mustMatchSecondSpecific() {
        List<SpecificAction> specifics = List.of(new TestSpecific(false, "test1"), new TestSpecific(true, "test2"));
        List<FallbackAction> fallback = List.of();

        var action = new ActionService(specifics, fallback, defaultAction);
        var result = action.resolve(MessageData.of()).block();

        assertThat(result).isNotNull();
        assertThat(result.getContent()).isEqualTo("test2");
    }

    @Test
    public void mustMatchFirstSpecificInListOfTwo() {
        List<SpecificAction> specifics = List.of(new TestSpecific(true, "test1"), new TestSpecific(false, "test2"));
        List<FallbackAction> fallback = List.of();

        var action = new ActionService(specifics, fallback, defaultAction);
        var result = action.resolve(MessageData.of()).block();

        assertThat(result).isNotNull();
        assertThat(result.getContent()).isEqualTo("test1");
    }

    @Test
    public void mustMatchFallback() {
        List<SpecificAction> specifics = List.of(new TestSpecific(false, "s1"), new TestSpecific(false, "s2"));
        List<FallbackAction> fallback = List.of(new TestFallback(true, "f1"));

        var action = new ActionService(specifics, fallback, defaultAction);
        var result = action.resolve(MessageData.of()).block();

        assertThat(result).isNotNull();
        assertThat(result.getContent()).isEqualTo("f1");
    }

    @Test
    public void mustMatchSecondFallback() {
        List<SpecificAction> specifics = List.of(new TestSpecific(false, "s1"), new TestSpecific(false, "s2"));
        List<FallbackAction> fallback = List.of(new TestFallback(false, "f1"), new TestFallback(true, "f2"));

        var action = new ActionService(specifics, fallback, defaultAction);
        var result = action.resolve(MessageData.of()).block();

        assertThat(result).isNotNull();
        assertThat(result.getContent()).isEqualTo("f2");
    }

    @Test
    public void mustMatchFirstFallbackInListOfTwo() {
        List<SpecificAction> specifics = List.of(new TestSpecific(false, "s1"), new TestSpecific(false, "s2"));
        List<FallbackAction> fallback = List.of(new TestFallback(false, "f1"), new TestFallback(false, "f2"));

        var action = new ActionService(specifics, fallback, defaultAction);
        var result = action.resolve(MessageData.of()).block();

        assertThat(result).isNotNull();
        assertThat(result.getContent()).isEqualTo("default");
    }

    @Test
    public void mustMatchDefault() {
        List<SpecificAction> specifics = List.of(new TestSpecific(false, "s1"), new TestSpecific(false, "s2"));
        List<FallbackAction> fallback = List.of(new TestFallback(true, "f1"), new TestFallback(false, "f2"));

        var action = new ActionService(specifics, fallback, defaultAction);
        var result = action.resolve(MessageData.of()).block();

        assertThat(result).isNotNull();
        assertThat(result.getContent()).isEqualTo("f1");
    }

    private static class TestSpecific implements SpecificAction {

        private final boolean supports;

        private final String val;

        public TestSpecific(boolean supports, String val) {
            this.supports = supports;
            this.val = requireNonNull(val);
        }

        @Override
        public boolean supports(MessageData data) {
            return supports;
        }

        @Override
        public Mono<MessageData> execute(MessageData data) {
            return Mono.just(MessageData.of().content(val));
        }
    }

    private static class TestFallback implements FallbackAction {

        private final boolean supports;

        private final String val;

        public TestFallback(boolean supports, String val) {
            this.supports = supports;
            this.val = requireNonNull(val);
        }

        @Override
        public boolean supports(MessageData data) {
            return supports;
        }

        @Override
        public Mono<MessageData> execute(MessageData data) {
            return Mono.just(MessageData.of().content(val));
        }
    }

    private static class TestDefaultAction implements DefaultAction {

        @Override
        public Mono<MessageData> execute(MessageData data) {
            return Mono.just(MessageData.of().content("default"));
        }
    }
}