package org.krjura.discord.lcwordcount.discord.actions.specific;

import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.krjura.discord.lcwordcount.db.Database;
import org.krjura.discord.lcwordcount.pojo.MessageData;
import org.mockito.ArgumentCaptor;

import static org.assertj.core.api.Assertions.assertThat;
import static org.krjura.discord.lcwordcount.discord.utils.TestUtils.mockSendMessage;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ChannelStatusActionTest {

    private static final String SELF_ROLE_ID = "statusRole1";
    private static final String CHANNEL_ID = "123456789";

    private Database database;

    private ChannelStatusAction action;

    @BeforeEach
    public void before() {
        this.database = mock(Database.class);
        this.action = new ChannelStatusAction(this.database);
    }

    @Test
    public void verifySupportWhenNotMentioned() {
        var roleOneData = MessageData
                .of()
                .selfGuildRoleIds(List.of(SELF_ROLE_ID))
                .roleMentionIds(List.of("not status"));
        assertThat(action.supports(roleOneData)).isFalse();
    }

    @Test
    public void verifySupportWhenMentionedButNoCommand() {
        var roleOneData = MessageData
                .of()
                .selfGuildRoleIds(List.of(SELF_ROLE_ID))
                .roleMentionIds(List.of(SELF_ROLE_ID))
                .content("not status content")
                .sanitizedContent("some status content");

        assertThat(action.supports(roleOneData)).isFalse();
    }

    @Test
    public void verifySupportWhenMentionedAndCommand() {
        var roleOneData = MessageData
                .of()
                .selfGuildRoleIds(List.of(SELF_ROLE_ID))
                .roleMentionIds(List.of(SELF_ROLE_ID))
                .content("status")
                .sanitizedContent("status");

        assertThat(action.supports(roleOneData)).isTrue();
    }

    @Test
    public void verifyExecute() {
        var sendMessageMocks = mockSendMessage();

        var data = MessageData
                .of()
                .message(sendMessageMocks.getMessage())
                .selfGuildRoleIds(List.of(SELF_ROLE_ID))
                .roleMentionIds(List.of(SELF_ROLE_ID))
                .channelId(CHANNEL_ID)
                .content("set min words 1")
                .sanitizedContent("set min words 1");

        when(this.database.isChannelRegistered(CHANNEL_ID)).thenReturn(true);
        when(this.database.getMinWords(CHANNEL_ID)).thenReturn(50);

        assertThat(action.execute(data).block()).isSameAs(data);

        ArgumentCaptor<String> textCaptor = ArgumentCaptor.forClass(String.class);
        verify(sendMessageMocks.getMessageChannel(), times(1)).createMessage(textCaptor.capture());

        assertThat(textCaptor.getValue()).contains("channel id: 123456789");
        assertThat(textCaptor.getValue()).contains("check active: yes");
        assertThat(textCaptor.getValue()).contains("min words: 50");
    }
}