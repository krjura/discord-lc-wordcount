/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.krjura.discord.lcwordcount.discord.actions.specific;

import org.krjura.discord.lcwordcount.pojo.MessageData;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import static java.lang.String.format;
import static org.krjura.discord.lcwordcount.discord.actions.utils.ActionUtils.isSelfMention;
import static org.krjura.discord.lcwordcount.discord.actions.utils.ActionUtils.sendMessage;

@Component
public class CommandsAction implements SpecificAction {

    private static final String COMMAND = "help";

    @Override
    public boolean supports(MessageData data) {
        if(!isSelfMention(data)) {
            return false;
        }

        return COMMAND.equals(data.getSanitizedContent());
    }

    @Override
    public Mono<MessageData> execute(MessageData data) {
        String reply = format(
                "<@%s> Bot supports the following commands: " +
                        "\n  status - shows configuration for this channel" +
                        "\n  check - activate check for this channel" +
                        "\n  no check - de-activate check for this channel" +
                        "\n  set min words <number> - set <number> as minimum number of words in the text",
                data.getMessage().getUserData().id()
        );

        return sendMessage(data, reply);
    }
}
