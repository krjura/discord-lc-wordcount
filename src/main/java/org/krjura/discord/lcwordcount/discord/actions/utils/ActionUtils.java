/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.krjura.discord.lcwordcount.discord.actions.utils;

import java.util.List;
import org.krjura.discord.lcwordcount.pojo.MessageData;
import reactor.core.publisher.Mono;

public class ActionUtils {

    private ActionUtils() {
        // util
    }

    public static boolean isSelfMention(MessageData msg) {
        List<String> roleMentionIds = msg.getRoleMentionIds();

        return msg
                .getSelfGuildRoleIds()
                .stream()
                .anyMatch(roleMentionIds::contains);
    }

    public static Mono<MessageData> sendMessage(MessageData data, String text) {
        return data
                .getMessage()
                .getChannel()
                .flatMap(mc -> mc.createMessage(text))
                .map(sendMsg -> data);
    }
}
