/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.krjura.discord.lcwordcount.discord.actions;

import org.krjura.discord.lcwordcount.db.Database;
import org.krjura.discord.lcwordcount.pojo.MessageData;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;

@Component
public class CountWordsAction implements DefaultAction {

    private final Database database;

    public CountWordsAction(Database database) {
        this.database = requireNonNull(database);
    }

    @Override
    public Mono<MessageData> execute(MessageData data) {
        final int minWords = database.getMinWords(data.getChannelId());

        int countWords = countWordsInAMessage(data);

        String discordReply = format(
                "<@%s>Too little words in this post expected %s but got %s",
                data.getMessage().getUserData().id(),
                minWords,
                countWords
        );

        if(countWords < minWords) {
            return data.getMessage()
                    .getChannel()
                    .flatMap(mc -> mc.createMessage(discordReply))
                    .map(sendMsg -> data);
        } else {
            return Mono.just(data);
        }
    }

    private int countWordsInAMessage(MessageData data) {
        String[] words = data.getContent().split("\\s+");

        return words.length;
    }
}
