/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.krjura.discord.lcwordcount.discord.actions.specific;

import org.krjura.discord.lcwordcount.db.Database;
import org.krjura.discord.lcwordcount.pojo.MessageData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;
import static org.krjura.discord.lcwordcount.discord.actions.utils.ActionUtils.isSelfMention;
import static org.krjura.discord.lcwordcount.discord.actions.utils.ActionUtils.sendMessage;

@Component
public class UnRegisterChanelAction implements SpecificAction {

    private static final Logger logger = LoggerFactory.getLogger(UnRegisterChanelAction.class);

    private static final String COMMAND = "no check";

    private final Database database;

    public UnRegisterChanelAction(Database database) {
        this.database = requireNonNull(database);
    }

    @Override
    public boolean supports(MessageData data) {
        if(!isSelfMention(data)) {
            return false;
        }

        return COMMAND.equals(data.getSanitizedContent());
    }

    @Override
    public Mono<MessageData> execute(MessageData data) {
        logger.info("{} - un-registering channel {}", data.getMessageId(), data.getChannelId() );

        this.database.unRegisterChannel(data.getChannelId());

        String discordReply = format(
                "<@%s> This channel will be no longer be checked for word length",
                data.getMessage().getUserData().id()
        );

        return sendMessage(data, discordReply);
    }
}
