/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.krjura.discord.lcwordcount.discord.actions.specific;

import org.krjura.discord.lcwordcount.db.Database;
import org.krjura.discord.lcwordcount.pojo.MessageData;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;
import static org.krjura.discord.lcwordcount.discord.actions.utils.ActionUtils.isSelfMention;

@Component
public class ChannelStatusAction implements SpecificAction {

    private static final String COMMAND = "status";

    private final Database database;

    public ChannelStatusAction(Database database) {
        this.database = requireNonNull(database);
    }

    @Override
    public boolean supports(MessageData data) {
        if(!isSelfMention(data)) {
            return false;
        }

        return COMMAND.equals(data.getSanitizedContent());
    }

    @Override
    public Mono<MessageData> execute(MessageData data) {
        String discordReply = format(
                "<@%s> channel info: " +
                        "\n channel id: %s" +
                        "\n check active: %s" +
                        "\n min words: %s",
                data.getMessage().getUserData().id(),
                data.getChannelId(),
                database.isChannelRegistered(data.getChannelId()) ? "yes" : "no",
                database.getMinWords(data.getChannelId())
        );

        return data
                .getMessage()
                .getChannel()
                .flatMap(mc -> mc.createMessage(discordReply))
                .map(sendMsg -> data);
    }
}
