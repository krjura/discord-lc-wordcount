/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.krjura.discord.lcwordcount.discord.actions.specific;

import org.krjura.discord.lcwordcount.db.Database;
import org.krjura.discord.lcwordcount.pojo.MessageData;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;
import static org.krjura.discord.lcwordcount.discord.actions.utils.ActionUtils.isSelfMention;
import static org.krjura.discord.lcwordcount.discord.actions.utils.ActionUtils.sendMessage;

@Component
public class ChangeMinWordsAction implements SpecificAction {

    private static final String COMMAND = "set min words";

    private final Database database;

    public ChangeMinWordsAction(Database database) {
        this.database = requireNonNull(database);
    }

    @Override
    public boolean supports(MessageData data) {
        if(!isSelfMention(data)) {
            return false;
        }

        return data.getSanitizedContent().startsWith(COMMAND);
    }

    @Override
    public Mono<MessageData> execute(MessageData data) {
        String content = data.getSanitizedContent();
        content = content.replace(COMMAND, "").strip();

        try {
            int minWords = Integer.parseInt(content);

            if(minWords < 1) {
                return sendMessage(
                        data,
                        format("<@%s> number must be larger then 0", data.getMessage().getUserData().id())
                );
            } else {
                this.database.setMinWords(data.getChannelId(), minWords);

                return sendMessage(
                        data,
                        format("<@%s> min words is now %s", data.getMessage().getUserData().id(), minWords)
                );
            }

        } catch (NumberFormatException e) {
            return sendMessage(
                    data,
                    format("<@%s> %s is not a number", data.getMessage().getUserData().id(), content)
            );
        }
    }
}