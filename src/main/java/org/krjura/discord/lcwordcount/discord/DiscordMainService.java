/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.krjura.discord.lcwordcount.discord;

import discord4j.common.util.Snowflake;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Message;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import org.krjura.discord.lcwordcount.db.Database;
import org.krjura.discord.lcwordcount.discord.actions.ActionService;
import org.krjura.discord.lcwordcount.discord.utils.DiscordFunctions;
import org.krjura.discord.lcwordcount.discord.utils.MessageDataUtils;
import org.krjura.discord.lcwordcount.pojo.MessageData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import static java.util.Objects.requireNonNull;

@Service
public class DiscordMainService {

    private static final Logger logger = LoggerFactory.getLogger(DiscordMainService.class);

    private final Database database;

    private final ActionService actionService;

    private final GatewayDiscordClient gateway;

    private String selfId;

    public DiscordMainService(
            Database database,
            ActionService actionService,
            GatewayDiscordClient gateway) {

        this.database = requireNonNull(database);
        this.actionService = requireNonNull(actionService);
        this.gateway = requireNonNull(gateway);
    }

    @PostConstruct
    public void init() {
        this.selfId = gateway.getSelf().block().getId().asString();

        start();
    }

    public void start() {
        gateway.on(MessageCreateEvent.class).subscribe(event -> {
            final Message message = event.getMessage();

            logger.info("{} - received message", message.getId().asString());

            Mono
                    .just(message)
                    .map(MessageDataUtils::initialize)
                    .map(DiscordFunctions::addAuthor)
                    .map(DiscordFunctions::addGuild)
                    .flatMap(this::addGuildSelf)
                    .flatMap(actionService::resolve)
                    .doOnError(Throwable::printStackTrace)
                    .doOnSuccess(this::log)
                    .subscribe();
        });
    }

    private Mono<MessageData> addGuildSelf(MessageData messageData) {
        if(messageData.getGuildId() == null) {
            return Mono.just(messageData);
        }

        List<String> guildSelfIds = database.getGuildSelfIds(messageData.getGuildId());
        if(guildSelfIds != null) {
            logger.info(
                    "{} - found self guild role ids {} for guild id {}",
                    messageData.getMessageId(),
                    guildSelfIds,
                    messageData.getGuildId()
            );
            return Mono.just(messageData.selfGuildRoleIds(guildSelfIds));
        }

        return gateway
                .getMemberById(Snowflake.of(messageData.getGuildId()), Snowflake.of(this.selfId))
                .map(member -> messageData.selfGuildRoleIds(member.getRoleIds().stream().map(Snowflake::asString).collect(Collectors.toList())))
                .doOnSuccess(msg -> this.database.setGuildSelfIds(msg.getGuildId(), msg.getSelfGuildRoleIds()))
                .switchIfEmpty(Mono.just(messageData));

    }

    private void log(MessageData msg) {
        if(msg == null) {
            logger.warn("Message data is null");
            return;
        }

        var duration = Duration.between(msg.getCreatedAt(), ZonedDateTime.now()).toMillis();

        logger.info(
                "{} - received message with: " +
                        "\n content: {}" +
                        "\n sanitized content: {}" +
                        "\n user: {}" +
                        "\n boot: {}" +
                        "\n guildId: {}" +
                        "\n guild role ids: {}" +
                        "\n processing took: {}",
                msg.getMessageId(),
                msg.getContent(),
                msg.getSanitizedContent(),
                msg.getUsername(),
                msg.isUserBot(),
                msg.getGuildId(),
                msg.getSelfGuildRoleIds(),
                duration
        );
    }
}