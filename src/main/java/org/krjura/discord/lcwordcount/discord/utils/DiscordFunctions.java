/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.krjura.discord.lcwordcount.discord.utils;

import discord4j.core.object.entity.User;
import org.krjura.discord.lcwordcount.pojo.MessageData;

public class DiscordFunctions {

    private DiscordFunctions() {
        // util
    }

    public static MessageData addAuthor(MessageData msg) {
        if(msg.getMessage().getAuthor().isPresent()) {
            User user = msg.getMessage().getAuthor().get();

            msg.username(user.getUsername());
            msg.userBot(user.isBot());

        }

        return msg;
    }

    public static MessageData addGuild(MessageData messageData) {
        messageData
                .getMessage()
                .getGuildId()
                .ifPresent(snowflake -> messageData.guildId(snowflake.asString()));

        return messageData;
    }

    public static boolean isUserBoot(MessageData msg) {
        return msg.isUserBot();
    }
}
