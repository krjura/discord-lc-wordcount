/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.krjura.discord.lcwordcount.discord.actions.fallback;

import org.krjura.discord.lcwordcount.db.Database;
import org.krjura.discord.lcwordcount.pojo.MessageData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import static java.util.Objects.requireNonNull;

@Component
public class IgnoreUnregisteredChannelAction implements FallbackAction {

    private static final Logger logger = LoggerFactory.getLogger(IgnoreUnregisteredChannelAction.class);

    private final Database database;

    public IgnoreUnregisteredChannelAction(Database database) {
        this.database = requireNonNull(database);
    }

    @Override
    public boolean supports(MessageData data) {
        return ! database.isChannelRegistered(data.getChannelId());
    }

    @Override
    public Mono<MessageData> execute(MessageData data) {

        logger.info("{} - ignoring message from an unregistered channel {}", data.getMessageId(), data.getChannelId());

        return Mono.just(data);
    }
}
