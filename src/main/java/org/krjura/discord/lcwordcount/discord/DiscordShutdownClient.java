/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.krjura.discord.lcwordcount.discord;

import discord4j.core.GatewayDiscordClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.context.SmartLifecycle;

import static java.util.Objects.requireNonNull;

@Service
public class DiscordShutdownClient implements SmartLifecycle {

    private static final Logger logger = LoggerFactory.getLogger(DiscordShutdownClient.class);

    private final GatewayDiscordClient gateway;

    private boolean isRunning = false;

    public DiscordShutdownClient(GatewayDiscordClient gateway) {
        this.gateway = requireNonNull(gateway);
    }

    @Override
    public int getPhase() {
        return Integer.MAX_VALUE;
    }

    @Override
    public void start() {
        logger.info("starting");
        this.isRunning = true;
    }

    @Override
    public void stop() {
        logger.info("stopping");
        this.gateway.logout().block();
        logger.info("stopped");
        this.isRunning = false;
    }

    @Override
    public boolean isAutoStartup() {
        return true;
    }

    @Override
    public boolean isRunning() {
        return this.isRunning;
    }
}
