/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.krjura.discord.lcwordcount.discord.actions;

import java.util.List;
import java.util.Optional;
import org.krjura.discord.lcwordcount.discord.actions.fallback.FallbackAction;
import org.krjura.discord.lcwordcount.discord.actions.specific.SpecificAction;
import org.krjura.discord.lcwordcount.pojo.MessageData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import static java.util.Objects.requireNonNull;

@Service
public class ActionService {

    private static final Logger logger = LoggerFactory.getLogger(ActionService.class);

    private final List<SpecificAction> specifics;

    private final List<FallbackAction> fallbacks;

    private final DefaultAction defaultAction;

    public ActionService(
            List<SpecificAction> specifics,
            List<FallbackAction> fallbacks,
            DefaultAction defaultAction) {

        this.specifics = requireNonNull(specifics);
        this.fallbacks = requireNonNull(fallbacks);
        this.defaultAction = requireNonNull(defaultAction);
    }

    public Mono<MessageData> resolve(MessageData data) {
        var action = findSpecific(data)
                .or(() -> findFallback(data))
                .orElse(defaultAction);

        logger.info(
                "{} - using action {}",
                data.getMessageId(),
                action.getClass().getSimpleName()
        );

        return action.execute(data);
    }

    private Optional<Action> findFallback(MessageData data) {
        return this
                .fallbacks
                .stream()
                .filter(action -> action.supports(data))
                .map(actions -> (Action) actions)
                .findFirst();
    }

    private Optional<Action> findSpecific(MessageData data) {
        return this
                .specifics
                .stream()
                .filter(action -> action.supports(data))
                .map(actions -> (Action) actions)
                .findFirst();
    }
}
