/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.krjura.discord.lcwordcount.discord.utils;

import discord4j.common.util.Snowflake;
import discord4j.core.object.entity.Message;
import java.io.CharArrayWriter;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.krjura.discord.lcwordcount.pojo.MessageData;

public class MessageDataUtils {

    private static final Pattern MENTION_PATTERN = Pattern.compile("<@&*!*\\d+>");
    private static final char NULL_CHAR = 0;

    private MessageDataUtils() {
        // util
    }

    public static MessageData initialize(Message msg) {
        return MessageData.of()
                .messageId(msg.getId().asString())
                .message(msg)
                .content(msg.getContent())
                .sanitizedContent(sanitizeMessageContent(msg.getContent()))
                .roleMentionIds(roleMentionIds(msg))
                .channelId(msg.getChannelId().asString());
    }

    private static List<String> roleMentionIds(Message msg) {
        return msg
                .getRoleMentionIds()
                .stream()
                .map(Snowflake::asString)
                .collect(Collectors.toList());
    }

    public static String sanitizeMessageContent(String content) {
        String newContent = sanitizeMessageContentMentions(content);

        return newContent.strip();
    }

    // TODO this should be optimized
    private static String sanitizeMessageContentMentions(String content) {
        char[] newContent = content.toCharArray();

        var mentionMatcher = MENTION_PATTERN.matcher(content);
        while (mentionMatcher.find()) {
            for(int i = mentionMatcher.start(); i < mentionMatcher.end(); i++) {
                newContent[i] = 0;
            }
        }

        CharArrayWriter writer = new CharArrayWriter(newContent.length);
        for(char contentChar : newContent) {
            if(contentChar != NULL_CHAR) {
                writer.append(contentChar);
            }
        }

        return writer.toString();
    }
}
