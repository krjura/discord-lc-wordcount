/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.krjura.discord.lcwordcount.pojo;

import discord4j.core.object.entity.Message;
import java.time.ZonedDateTime;
import java.util.List;

public class MessageData {

    private ZonedDateTime createdAt;

    private String messageId;

    private Message message;

    private String content;

    private String sanitizedContent;

    private String username;

    private boolean isUserBot;

    private String guildId;

    private List<String> selfGuildRoleIds;

    private List<String> roleMentionIds;

    private String channelId;

    public MessageData() {
        this.createdAt = ZonedDateTime.now();
        this.selfGuildRoleIds = List.of();
        this.roleMentionIds = List.of();
    }

    public static MessageData of() {
        return new MessageData();
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public String getMessageId() {
        return messageId;
    }

    public Message getMessage() {
        return message;
    }

    public String getContent() {
        return content;
    }

    public String getSanitizedContent() {
        return sanitizedContent;
    }

    public String getUsername() {
        return username;
    }

    public boolean isUserBot() {
        return isUserBot;
    }

    public String getGuildId() {
        return guildId;
    }

    public List<String> getSelfGuildRoleIds() {
        return selfGuildRoleIds;
    }

    public List<String> getRoleMentionIds() {
        return roleMentionIds;
    }

    public String getChannelId() {
        return channelId;
    }

    public MessageData messageId(final String messageId) {
        this.messageId = messageId;
        return this;
    }

    public MessageData message(final Message message) {
        this.message = message;
        return this;
    }

    public MessageData content(final String content) {
        this.content = content;
        return this;
    }

    public MessageData sanitizedContent(final String sanitizedContent) {
        this.sanitizedContent = sanitizedContent;
        return this;
    }

    public MessageData username(final String username) {
        this.username = username;
        return this;
    }

    public MessageData userBot(final boolean userBot) {
        this.isUserBot = userBot;
        return this;
    }

    public MessageData guildId(final String guildId) {
        this.guildId = guildId;
        return this;
    }

    public MessageData selfGuildRoleIds(final List<String> selfGuildRoleIds) {
        this.selfGuildRoleIds = selfGuildRoleIds == null ? List.of() : selfGuildRoleIds;
        return this;
    }

    public MessageData roleMentionIds(final List<String> roleMentionIds) {
        this.roleMentionIds = roleMentionIds == null ? List.of() : roleMentionIds;
        return this;
    }

    public MessageData channelId(final String channelId) {
        this.channelId = channelId;
        return this;
    }
}
