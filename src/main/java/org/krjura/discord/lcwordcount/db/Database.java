/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.krjura.discord.lcwordcount.db;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import org.krjura.discord.lcwordcount.config.AppConfig;
import org.krjura.discord.lcwordcount.ex.ApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class Database {

    private static final Logger logger = LoggerFactory.getLogger(Database.class);

    private final AppConfig appConfig;

    private final DatabaseData data;

    private final ObjectMapper mapper;

    public Database(AppConfig appConfig) {
        this.appConfig = appConfig;

        this.mapper = new ObjectMapper();
        this.mapper.enable(SerializationFeature.INDENT_OUTPUT);

        this.data = loadDatabase();
    }

    // persistence
    public DatabaseData loadDatabase() {
        var loc = new File(this.appConfig.getBackupLocation());

        // if database does not exists create new one
        if(! loc.exists()) {
             return new DatabaseData();
        }

        try {
            return this.mapper.readValue(loc, DatabaseData.class);
        } catch (IOException e) {
            throw new ApplicationException("Cannot load database from " + this.appConfig.getBackupLocation(), e);
        }
    }

    @Scheduled(fixedRate = 60_000, initialDelay = 30_000)
    public void backup() {
        logger.info("persisting database to {}", this.appConfig.getBackupLocation());

        try(FileOutputStream fis = new FileOutputStream(this.appConfig.getBackupLocation())) {
            fis.write(this.mapper.writeValueAsBytes(data.copy()));
        } catch (JsonProcessingException e) {
            logger.warn("Cannot save database as JSON", e);
        } catch (IOException e ) {
            logger.warn("cannot save database", e);
        }
    }


    public List<String> getGuildSelfIds(String guildId) {
        return data.getGuildSelfIds().get(guildId);
    }

    public void setGuildSelfIds(String guildId, List<String> selfIds) {
        this.data.getGuildSelfIds().put(guildId, selfIds);
    }

    public boolean isChannelRegistered(String channelId) {
        return data
                .getChannelRegistrations()
                .getOrDefault(channelId, ChannelInfo.defaultInfo())
                .isRegistered();
    }

    public void registerChannel(String channelId) {
        this
                .data
                .getChannelRegistrations()
                .computeIfAbsent(channelId, key -> ChannelInfo.defaultInfo())
                .registered(true);
    }

    public void unRegisterChannel(String channelId) {
        this
                .data
                .getChannelRegistrations()
                .computeIfAbsent(channelId, key -> ChannelInfo.defaultInfo())
                .registered(false);
    }

    public void setMinWords(String channelId, int minWords) {
        this
                .data
                .getChannelRegistrations()
                .computeIfAbsent(channelId, key -> ChannelInfo.defaultInfo())
                .minWords(minWords);
    }

    public int getMinWords(String channelId) {
        return this
                .data
                .getChannelRegistrations()
                .computeIfAbsent(channelId, key -> ChannelInfo.defaultInfo())
                .getMinWords();
    }
}
