/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.krjura.discord.lcwordcount.db;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ChannelInfo {

    private static final ChannelInfo DEFAULT = ChannelInfo.of();

    private boolean registered;

    private int minWords;

    private ChannelInfo() {
        this(false, 50);
    }

    private ChannelInfo(boolean registered, int minWords) {
        this.registered = registered;
        this.minWords = minWords;
    }

    public static ChannelInfo of() {
        return new ChannelInfo();
    }

    public static ChannelInfo defaultInfo() {
        return DEFAULT.copy();
    }

    public ChannelInfo copy() {
        return new ChannelInfo(this.registered, this.minWords);
    }

    public boolean isRegistered() {
        return registered;
    }

    public void setRegistered(boolean registered) {
        this.registered = registered;
    }

    public int getMinWords() {
        return minWords;
    }

    public void setMinWords(int minWords) {
        this.minWords = minWords;
    }

    // fluent
    public ChannelInfo registered(final boolean registered) {
        this.registered = registered;
        return this;
    }

    public ChannelInfo minWords(final int maxWords) {
        this.minWords = maxWords;
        return this;
    }
}
