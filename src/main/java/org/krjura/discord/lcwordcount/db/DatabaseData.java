/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.krjura.discord.lcwordcount.db;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DatabaseData {

    private final Map<String, List<String>> guildSelfIds;

    private final Map<String, ChannelInfo> channelInfo;

    public DatabaseData() {
        this.guildSelfIds = new ConcurrentHashMap<>();
        this.channelInfo = new ConcurrentHashMap<>();
    }

    @JsonCreator
    public DatabaseData(
            @JsonProperty("guildSelfIds") Map<String, List<String>> guildSelfIds,
            @JsonProperty("channelRegistrations") Map<String, ChannelInfo> channelRegistrations) {

        var copy = copy(
                guildSelfIds == null ? new HashMap<>() : guildSelfIds,
                channelRegistrations == null ? new HashMap<>() : channelRegistrations
        );

        this.guildSelfIds = copy.guildSelfIds;
        this.channelInfo = copy.channelInfo;
    }

    public DatabaseData(
            ConcurrentHashMap<String, List<String>> guildSelfIds,
            ConcurrentHashMap<String, ChannelInfo> channelRegistrations) {

        this.guildSelfIds = guildSelfIds == null ? new ConcurrentHashMap<>() : guildSelfIds;
        this.channelInfo = channelRegistrations == null ? new ConcurrentHashMap<>() : channelRegistrations;
    }

    DatabaseData copy() {
        return copy(this.guildSelfIds, this.channelInfo);
    }

    DatabaseData copy(Map<String, List<String>> guildSelfIds, Map<String, ChannelInfo> channelRegistrations) {

        var guildSelfIdsCopy = new ConcurrentHashMap<String, List<String>>(guildSelfIds.size());
        for(var entry : guildSelfIds.entrySet()) {
            guildSelfIdsCopy.put(entry.getKey(), copy(entry.getValue()));
        }

        var channelRegistrationsCopy = new ConcurrentHashMap<String, ChannelInfo>(channelRegistrations.size());
        for(var entry :channelRegistrations.entrySet()) {
            channelRegistrationsCopy.put(entry.getKey(), entry.getValue().copy());
        }

        return new DatabaseData(guildSelfIdsCopy, channelRegistrationsCopy);
    }

    private List<String> copy(List<String> val) {
        List<String> copy = new ArrayList<>(val.size());
        copy.addAll(val);

        return copy;
    }

    public Map<String, List<String>> getGuildSelfIds() {
        return guildSelfIds;
    }

    public Map<String, ChannelInfo> getChannelRegistrations() {
        return channelInfo;
    }
}
